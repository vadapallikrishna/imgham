from app import app
import os

PORT = int(os.getenv('APP_PORT','3000'))

if __name__ == "__main__":
  app.run(port=PORT) 
