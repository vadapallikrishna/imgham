import os
from flask import Flask
import unittest
import tempfile

class TestHelper(unittest.TestCase):


   def setUp(self):
       app = Flask(__name__)
       self.db_fd,app.config['DATABASE'] = tempfile.mkstemp()
       app.config['TESTING'] = True
       self.app = app.test_client()
       

   def tearDown(self):
      os.close(self.db_fd)
      os.unlink(app.config['DATABASE'])

if __name__ == '__main__':
    unittest.main()
